//
//  CustomCell.swift
//  ono
//
//  Created by Szabolcs Toth on 8/10/16.
//  Copyright © 2016 purzelbaum.hu. All rights reserved.
//

import Cocoa

class CustomCell: NSTableCellView {
    
    @IBOutlet var cellDescription: NSTextField!
    @IBOutlet var cellFeature: NSTextField!
    @IBOutlet var cellQuantity: NSTextField!
    @IBOutlet var cellImage: NSImageView!

    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)

        // Drawing code here.
    }
    
}
