//
//  MainWindowController.swift
//  ono
//
//  Created by Szabolcs Toth on 8/10/16.
//  Copyright © 2016 purzelbaum.hu. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController, NSToolbarDelegate {
    
    var toolbar: NSToolbar!
    
    let toolbarItems: [[String: String]] = [
        ["title" : "operations", "icon": "NSPreferencesGeneral", "identifier": "NavigationGroupToolbarItem"]
    ]
    
    var toolbarTabsIdentifiers: [String] {
        
        return toolbarItems.flatMap { $0["identifier"] }
    }

    override func windowDidLoad() {
        super.windowDidLoad()
        
        self.configureWindowAppearance()
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    // MARK: - Customizing NSWindow
    private func configureWindowAppearance() {
        if let window = window {
            if let view = window.contentView {
                view.wantsLayer = true
            }
            
            window.styleMask |= NSFullSizeContentViewWindowMask
            window.titleVisibility = .Hidden
            window.titlebarAppearsTransparent = true
            
            toolbar = NSToolbar(identifier: "TheToolbarIdentifier")
            toolbar.allowsUserCustomization = true
            toolbar.delegate = self
            
            self.window?.toolbar = toolbar
        }
    }
  
    // MARK: - Toolbar
    func toolbar(toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: String, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        
         guard let infoDictionary: [String : String] = toolbarItems.filter({ $0["identifier"] == itemIdentifier }).first
            else { return nil }
        
        let toolbarItem: NSToolbarItem
        
        if itemIdentifier == "NavigationGroupToolbarItem" {
            
            let group = NSToolbarItemGroup(itemIdentifier: itemIdentifier)
            
            let itemA = NSToolbarItem(itemIdentifier: "LoadToolbarItem")
            itemA.label = "Load"
            let itemB = NSToolbarItem(itemIdentifier: "SaveToolbarItem")
            itemB.label = "Save"
            
            let segmented = NSSegmentedControl(frame: NSRect(x: 0, y: 0, width: 205, height: 40))
            segmented.segmentStyle = .TexturedRounded
            segmented.trackingMode = .Momentary
            segmented.segmentCount = 2
            // Don't set a label: these would appear inside the button
            segmented.setImage(Onoicons.imageOfOpeningFile, forSegment: 0)
            segmented.setLabel("Load", forSegment: 0)
            segmented.setWidth(100, forSegment: 0)
            segmented.setLabel("Save", forSegment: 1)
            segmented.setImage(Onoicons.imageOfSavingFile, forSegment: 1)
            segmented.setWidth(100, forSegment: 1)
            
            
            // `group.label` would overwrite segment labels
            group.paletteLabel = "Navigation"
            group.subitems = [itemA, itemB]
            group.view = segmented
            
            toolbarItem = group
            toolbarItem.action = #selector(ViewController.buttonPressed(_:))
        } else {
            toolbarItem = NSToolbarItem(itemIdentifier: itemIdentifier)
            toolbarItem.label = infoDictionary["title"]!
            
            let iconImage = NSImage(named: infoDictionary["icon"]!)
            let button = NSButton(frame: NSRect(x: 0, y: 0, width: 40, height: 40))
            button.image = iconImage
            button.bezelStyle = .TexturedRoundedBezelStyle
            toolbarItem.view = button
        }
        return toolbarItem
    }
    
    func toolbarDefaultItemIdentifiers(toolbar: NSToolbar) -> [String] {
        
        return self.toolbarTabsIdentifiers;
    }
    
    func toolbarAllowedItemIdentifiers(toolbar: NSToolbar) -> [String] {
        
        return self.toolbarDefaultItemIdentifiers(toolbar)
    }
    
    func toolbarSelectableItemIdentifiers(toolbar: NSToolbar) -> [String] {
        
        return self.toolbarDefaultItemIdentifiers(toolbar)
    }
    
    func toolbarWillAddItem(notification: NSNotification) {
        
 //       print("toolbarWillAddItem", (notification.userInfo?["item"] as? NSToolbarItem)?.itemIdentifier ?? "")
    }
    
    func toolbarDidRemoveItem(notification: NSNotification) {
        
 //       print("toolbarDidRemoveItem", (notification.userInfo?["item"] as? NSToolbarItem)?.itemIdentifier ?? "")
        
    }

}
