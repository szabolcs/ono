//
//  ViewController.swift
//  ono
//
//  Created by Szabolcs Toth on 8/10/16.
//  Copyright © 2016 purzelbaum.hu. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    @IBOutlet var tableView: NSTableView!
    var path : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.refreshTableView), name: "refresh", object: nil)
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    // MARK: Set ViewController background to white, so the toolvar will be white as it is transparent
    override func awakeFromNib() {
        if self.view.layer != nil {
            let whiteColor = NSColor.whiteColor()
            self.view.layer?.backgroundColor = whiteColor.CGColor
        }
    }
    
    // MARK: - NotificationCenter
    func refreshTableView() {
        self.tableView.reloadData()
    }

    // MARK: - Handling Toolbar buttons
    @IBAction func buttonPressed(sender: AnyObject) {
        
        switch(sender.selectedSegment) {
            case 0:
                openFromFile()
            
            case 1:
                saveToFile()
                
            default:
                break
        }
        
    }
    
    // MARK: - TableView
    
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        return responseArray().count
    }
    
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cell = tableView.makeViewWithIdentifier("cell", owner: self) as! CustomCell
        
        cell.cellDescription.stringValue = responseArray().objectAtIndex(row)["DESC"] as! String
        cell.cellFeature.stringValue = responseArray().objectAtIndex(row)["FC"] as! String
        cell.cellQuantity.stringValue = responseArray().objectAtIndex(row)["QTY"] as! String
        cell.cellImage.image = nil
        

        if let type = responseArray().objectAtIndex(row)["TYPE"] as? String {
            if type == "Hardware" {
                cell.cellImage.image = Onoicons.imageOfHardware
            }
            else {
                cell.cellImage.image = Onoicons.imageOfSoftware
            }
        }

        return cell
    }

    
    // MARK: - Open dialog
    func openFromFile() {
        
        let dialog = NSOpenPanel()
        
        dialog.title                                      = "Choose a .xml file";
        dialog.showsResizeIndicator          = true;
        dialog.showsHiddenFiles               = false;
        dialog.canChooseDirectories         = true;
        dialog.canCreateDirectories           = true;
        dialog.allowsMultipleSelection      = false;
        dialog.allowedFileTypes                 = ["xml"];
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.URL
            
            if (result != nil) {
                path = result!.path!
                
                processXML(path)
                
            } else {
                // User clicked on "Cancel"
                return
            }
        }
        NSNotificationCenter.defaultCenter().postNotificationName("refresh", object: nil)
    }

    func saveToFile() {
        
        let preArray: NSMutableArray = []
        
        if responseArray().count != 0 {
            
            // Prepare array for writing
            for line in responseArray() {
                let FC = line["FC"] as! String
                let DESC  = line["DESC"] as! String
                let QTY  = line["QTY"] as! String
                preArray.addObject("\(FC)\t\(DESC)\t\(QTY)")
            }
            
            let arrayToWrite = preArray as AnyObject as! [String]
            
            let joined = arrayToWrite.joinWithSeparator("\n")
        
            // Opening save panel
            let savePanel = NSSavePanel()
            let filename = (path as NSString).lastPathComponent
            savePanel.nameFieldStringValue = "\(filename).tsv"
            savePanel.beginWithCompletionHandler { (result: Int) -> Void in
                if result == NSFileHandlingPanelOKButton {
                    let exportedFileURL = savePanel.URL
                    let data = joined.dataUsingEncoding(NSUTF8StringEncoding)
                    
                    NSFileManager.defaultManager().createFileAtPath(exportedFileURL!.path!, contents: data, attributes: nil)
                }
            }
        }
        else {
            let alert = NSAlert()
            alert.messageText = "No XML, no PARTY!"
            alert.addButtonWithTitle("OK")
            alert.informativeText = "Please, load an .xml file first."
            
            alert.beginSheetModalForWindow(self.view.window!, completionHandler: nil )
        }
    }
}

