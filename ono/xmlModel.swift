//
//  xmlModel.swift
//  XML2BOQ
//
//  Created by Szabolcs Toth on 7/1/16.
//  Copyright © 2016 purzelbaum.hu. All rights reserved.
//

import Foundation

var configuration: NSMutableArray = []

func processXML (result: String) {
    
    let path = result
    
    let data = NSData(contentsOfFile: path)
    
    // Delete everything from the array
    configuration = []
    
    do {
        let xmlDoc = try AEXMLDocument(xmlData: data!)
        
        if let lines = xmlDoc.root["CFData"]["ProductLineItem"].all {
            
            for line in lines {
                if let name = line["ProductIdentification"]["PartnerProductIdentification"]["ProprietaryProductIdentifier"].value {
                    let descriptionName = line["ProductIdentification"]["PartnerProductIdentification"]["ProductDescription"].value
                    let productType = line["ProductIdentification"]["PartnerProductIdentification"]["ProductTypeCode"].value
                    
                    var productSerial = ""
                    
                    if (line["ProductIdentification"]["PartnerProductIdentification"]["OrderedProductIdentifier"].value! != "N/A") {
                        
                        productSerial = line["ProductIdentification"]["PartnerProductIdentification"]["OrderedProductIdentifier"].value!
                        
                    }
                    
                    if (line["TransactionType"].value! == "NEW" || line["TransactionType"].value! == "FMOD") {
                        
                        let configurationMainLines = [
                            "FC" : "\(name)",
                            "DESC": "\(descriptionName!)",
                            // TODO
                            "MAIN" : "true",
                            "QTY" : "\(productSerial)",
                            "TYPE": "\(productType!)"
                        ]
                        
                        configuration.addObject(configurationMainLines)
                    }
                    
                    
                    if let sublines = line["ProductSubLineItem"].all {
                        for subline in sublines {
                            let description = subline["ProductIdentification"]["PartnerProductIdentification"]["ProductDescription"].value
                            let featureCode = subline["ProductIdentification"]["PartnerProductIdentification"]["ProprietaryProductIdentifier"].value
                            
                            let quantity = subline["Quantity"].value
                            
                            if(subline["TransactionType"].value! == "ADD") {
                                
                                let configurationLines = [
                                    "FC" : "\(featureCode!)",
                                    "DESC": "\(description!)",
                                    // TODO
                                    "MAIN" : "false",
                                    "QTY" : "\(quantity!)"
                                ]
                                
                                configuration.addObject(configurationLines)
                                
                            }
                        }
                    }
                }
            }
        }
    }
        
    catch {
        print("\(error)")
    }
  
}

func responseArray() -> NSMutableArray {
    return configuration
}
